<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Resources\Product as ProductResource;

class ProductController extends Controller
{
    public function index()
    {
        //Get all products
        $product = Product::paginate(15);

        // Return a collection of $product with pagination
        return ProductResource::collection($product);
    }

    public function show($id)
    {
        //Get the product
        $product = Product::findOrfail($id);

        // Return a single product
        return new ProductResource($product);
    }

    public function destroy($id)
    {
        //Get the product
        $product = Product::findOrfail($id);

        if($product->delete()) {
            return new ProductResource($product);
        }
    }

    public function store(Request $request)
    {
        $product = $request->isMethod('put') ? Product::findOrFail($request->id) : new Product();

        $product->id = $request->input('id');
        $product->name = $request->input('name');

        if($product->save()) {
            return new ProductResource($product);
        }
    }
}
