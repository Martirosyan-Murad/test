<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// get list of products
Route::get('products','ProductController@index');
// get specific product
Route::get('product/{id}','ProductController@show');
// create new product
Route::post('product','ProductController@store');
// update existing product
Route::put('product','ProductController@store');
// delete a product
Route::delete('product/{id}','ProductController@destroy');
